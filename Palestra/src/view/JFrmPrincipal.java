package view;
import Model.Usuario;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class JFrmPrincipal extends javax.swing.JFrame {
    Usuario UsuarioLogago;
    public JFrmPrincipal(Usuario usuario) {
        initComponents();
        this.setLocationRelativeTo(null);
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.setIconImage(new ImageIcon(getClass().getResource("/images/icon-frames.png")).getImage());
        this.UsuarioLogago = usuario;
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        dpPrincipal = new javax.swing.JDesktopPane();
        mPrincipal = new javax.swing.JMenuBar();
        mCadastros = new javax.swing.JMenu();
        mItemEvento = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        mRelatorio = new javax.swing.JMenu();
        mOpcoes = new javax.swing.JMenu();
        mConfig = new javax.swing.JMenuItem();
        mSobre = new javax.swing.JMenuItem();
        mSair = new javax.swing.JMenuItem();

        jMenu1.setText("jMenu1");

        jMenu2.setText("jMenu2");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Talk Me - Principal");

        javax.swing.GroupLayout dpPrincipalLayout = new javax.swing.GroupLayout(dpPrincipal);
        dpPrincipal.setLayout(dpPrincipalLayout);
        dpPrincipalLayout.setHorizontalGroup(
            dpPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 829, Short.MAX_VALUE)
        );
        dpPrincipalLayout.setVerticalGroup(
            dpPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 473, Short.MAX_VALUE)
        );

        mCadastros.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/application_add.png"))); // NOI18N
        mCadastros.setText("Cadastros");
        mCadastros.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        mItemEvento.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        mItemEvento.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/group.png"))); // NOI18N
        mItemEvento.setText("Evento");
        mItemEvento.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        mItemEvento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mItemEventoActionPerformed(evt);
            }
        });
        mCadastros.add(mItemEvento);

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/script.png"))); // NOI18N
        jMenuItem2.setText("Palestra");
        jMenuItem2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        mCadastros.add(jMenuItem2);

        jMenuItem3.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/user_suit.png"))); // NOI18N
        jMenuItem3.setText("Palestrante");
        jMenuItem3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        mCadastros.add(jMenuItem3);

        mPrincipal.add(mCadastros);

        mRelatorio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/printer.png"))); // NOI18N
        mRelatorio.setText("Relatorio");
        mRelatorio.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        mRelatorio.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                mRelatorioMouseClicked(evt);
            }
        });
        mPrincipal.add(mRelatorio);

        mOpcoes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/application_xp_terminal.png"))); // NOI18N
        mOpcoes.setText("Opções");
        mOpcoes.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        mConfig.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_MASK));
        mConfig.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/cog.png"))); // NOI18N
        mConfig.setText("Configurações");
        mConfig.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        mConfig.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mConfigActionPerformed(evt);
            }
        });
        mOpcoes.add(mConfig);

        mSobre.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        mSobre.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/information.png"))); // NOI18N
        mSobre.setText("Sobre");
        mSobre.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        mSobre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mSobreActionPerformed(evt);
            }
        });
        mOpcoes.add(mSobre);

        mSair.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_ESCAPE, 0));
        mSair.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/door_in.png"))); // NOI18N
        mSair.setText("Sair");
        mSair.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        mSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mSairActionPerformed(evt);
            }
        });
        mOpcoes.add(mSair);

        mPrincipal.add(mOpcoes);

        setJMenuBar(mPrincipal);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(dpPrincipal)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(dpPrincipal)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    public void controlabotoes(boolean Habilitar){     
        mCadastros.enable(Habilitar);
        mRelatorio.enable(Habilitar);
        mOpcoes.enable(Habilitar);
        mSair.enable(Habilitar);
    }

    
    private void mItemEventoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mItemEventoActionPerformed
        JIFrmEvento tela = new JIFrmEvento(this);    
        tela.setVisible(true);
        controlabotoes(false);
        dpPrincipal.add(tela);
    }//GEN-LAST:event_mItemEventoActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        JIFrmPalestra tela = new JIFrmPalestra(this, this.UsuarioLogago);    
        tela.setVisible(true);
        controlabotoes(false);
        dpPrincipal.add(tela);
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void mConfigActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mConfigActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_mConfigActionPerformed

    private void mSobreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mSobreActionPerformed
        JIFrmSobre sobre = new JIFrmSobre(this);
        sobre.setVisible(true);
        controlabotoes(false);
        dpPrincipal.add(sobre);
    }//GEN-LAST:event_mSobreActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        JIFrmPalestrante palestrante = new JIFrmPalestrante(this);
        palestrante.setVisible(true);
        dpPrincipal.add(palestrante);
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void mRelatorioMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mRelatorioMouseClicked
        JIFrmFiltroRelatorio filtro = new JIFrmFiltroRelatorio();
        filtro.setVisible(true);
        dpPrincipal.add(filtro);
    }//GEN-LAST:event_mRelatorioMouseClicked

    private void mSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mSairActionPerformed
        this.dispose();
    }//GEN-LAST:event_mSairActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JDesktopPane dpPrincipal;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    public javax.swing.JMenu mCadastros;
    private javax.swing.JMenuItem mConfig;
    public javax.swing.JMenuItem mItemEvento;
    private javax.swing.JMenu mOpcoes;
    private javax.swing.JMenuBar mPrincipal;
    private javax.swing.JMenu mRelatorio;
    private javax.swing.JMenuItem mSair;
    private javax.swing.JMenuItem mSobre;
    // End of variables declaration//GEN-END:variables
}
