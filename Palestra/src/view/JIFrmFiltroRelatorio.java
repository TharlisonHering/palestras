package view;

import Dao.PalestraDao;
import JDBC.EventoDaoJDBC;
import JDBC.PalestraDaoJDBC;
import JDBC.RelatorioDaoJDBC;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class JIFrmFiltroRelatorio extends javax.swing.JInternalFrame {

    public JIFrmFiltroRelatorio() {
        initComponents();
        this.setLocation(100,100);
        AlimentarComboEventos();
        AlimentarComboPalestras(-1);
    
    }

    public void AlimentarComboEventos(){
        EventoDaoJDBC eventos = new EventoDaoJDBC();
        ResultSet rs = eventos.Listar();
        try{
            while(rs.next()){
                cbEventos.addItem(rs.getString("NOME"));
            }
        }catch(Exception ex){
                JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void AlimentarComboPalestras(int Evento){
        cbPalestras.removeAllItems();
        cbPalestras.addItem("Todos");
        
        PalestraDaoJDBC palestra = new PalestraDaoJDBC();
        ResultSet rs = palestra.BuscarPalestraPorEvento(Evento);
        try{
            while(rs.next()){
                cbPalestras.addItem(rs.getString("NOME"));
            }
        }catch(Exception ex){
                JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        cbEventos = new javax.swing.JComboBox();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        cbPalestras = new javax.swing.JComboBox();
        btnCancelar = new javax.swing.JButton();
        btnSalvar = new javax.swing.JButton();

        setTitle("Talk Me - Relatório de Satisfação");

        cbEventos.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Todos" }));
        cbEventos.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cbEventos.setName(""); // NOI18N
        cbEventos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                cbEventosMouseExited(evt);
            }
        });
        cbEventos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbEventosActionPerformed(evt);
            }
        });

        jLabel8.setText("Evento");

        jLabel9.setText("Palestra");

        cbPalestras.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Todos" }));
        cbPalestras.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/door_in.png"))); // NOI18N
        btnCancelar.setText("Sair");
        btnCancelar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        btnSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/page_white_acrobat.png"))); // NOI18N
        btnSalvar.setText("Gerar");
        btnSalvar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(btnCancelar)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(btnSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel9)
                            .addGap(18, 18, 18)
                            .addComponent(cbPalestras, javax.swing.GroupLayout.PREFERRED_SIZE, 348, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addGap(23, 23, 23)
                        .addComponent(cbEventos, javax.swing.GroupLayout.PREFERRED_SIZE, 349, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(26, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbEventos, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addGap(27, 27, 27)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(cbPalestras, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnSalvar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        RelatorioDaoJDBC relatorio = new RelatorioDaoJDBC();
        int idEvento   = 0;
        int idPalestra = 0;

        if(!cbEventos.getSelectedItem().equals("Todos")){
            EventoDaoJDBC eventos = new EventoDaoJDBC();
            ResultSet rs = eventos.Listar();
            try{
                while(rs.next()){
                    if(cbEventos.getSelectedItem().equals(rs.getString("NOME"))){
                        idEvento = rs.getInt("CODIGO");
                    }
                }
                
                if(!cbPalestras.getSelectedItem().equals("Todos")){
                    PalestraDaoJDBC palestras = new PalestraDaoJDBC();
                    ResultSet res = palestras.Listar();
                    while(res.next()){
                        if(cbPalestras.getSelectedItem().equals(rs.getString("NOME"))){
                            idPalestra = res.getInt("CODIGO");
                        }
                    }
                    relatorio.GerarPdf(idEvento,idPalestra);
                }else{
                    relatorio.GerarPdf(idEvento,0);
                }
                
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, "Erro: "+ ex.getMessage());
            }
        }else{
            relatorio.GerarPdf(0,0);
        }
        
    }//GEN-LAST:event_btnSalvarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void cbEventosMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cbEventosMouseExited
       
    }//GEN-LAST:event_cbEventosMouseExited

    private void cbEventosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbEventosActionPerformed
         if(!cbEventos.getSelectedItem().equals("Todos")){
            EventoDaoJDBC eventos = new EventoDaoJDBC();
            ResultSet rs = eventos.Listar();
            try{
                while(rs.next()){
                    if(cbEventos.getSelectedItem().equals(rs.getString("NOME"))){
                        AlimentarComboPalestras(rs.getInt("CODIGO"));
                    }
                }
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, "Erro: "+ ex.getMessage());
            }
        }
    }//GEN-LAST:event_cbEventosActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JComboBox cbEventos;
    private javax.swing.JComboBox cbPalestras;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
