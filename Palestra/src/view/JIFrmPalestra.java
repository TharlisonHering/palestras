package view;

import JDBC.PalestraDaoJDBC;
import Model.Palestra;
import Model.Usuario;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import net.proteanit.sql.DbUtils;

public class JIFrmPalestra extends javax.swing.JInternalFrame {
    
    JFrmPrincipal principal;
    int CodigoPalestrante = -1;
    Usuario usuarioLogado;
    
    public JIFrmPalestra(JFrmPrincipal pai, Usuario usuario) {
        initComponents();
        this.principal = pai;
        this.usuarioLogado = usuario;
        
        ListarPalestras();
        ListarPalestrantes();
        
        LimpaComboBoxEventos();
        PopulaComboBoxEventos();
        LimpaComboBoxPalestrantes();
        PopulaComboBoxPalestrantes();
    }
    
    public void ListarPalestras(){
        tblPalestras.setModel(DbUtils.resultSetToTableModel(new PalestraDaoJDBC().Listar()));
    }
    
    public void ListarPalestrantes(){
        int palestraID = -1;
        if(!txtCodigo.getText().toLowerCase().isEmpty())
            palestraID = Integer.parseInt(txtCodigo.getText());
        tblPalestrantes.setModel(DbUtils.resultSetToTableModel(new PalestraDaoJDBC().BuscarPalestrantesDaPalestra(palestraID)));
    }
    
    public void LimparCampos(){
        txtCodigo.setText("");
        txtNome.setText("");
        txtData.setText("");
        cbEventos.setSelectedIndex(0);
    }
    
    public void PopulaComboBoxEventos(){
        try{
            PalestraDaoJDBC PalestraDao = new PalestraDaoJDBC();
            ResultSet rs = PalestraDao.BuscarEventos();
            while(rs.next()){
                cbEventos.addItem(rs.getString("Nome"));
            }
       }catch(Exception ex){
                JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void LimpaComboBoxEventos(){
        for(int i = cbEventos.getItemCount(); i > 1; i--){
            cbEventos.remove(i);
        }
    }
    
    public void PopulaComboBoxPalestrantes(){
        try{
            PalestraDaoJDBC PalestraDao = new PalestraDaoJDBC();
            ResultSet rs = PalestraDao.BuscarPalestrantes();
            while(rs.next()){
                cbPalestrante.addItem(rs.getString("Nome"));
            }
       }catch(Exception ex){
                JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
       }
    }
    
    public void LimpaComboBoxPalestrantes(){
        for(int i = cbPalestrante.getItemCount(); i > 1; i--){
            cbPalestrante.remove(i);
        }
    }
    
    public void CarregarDoGrid(){
        int Codigo = tblPalestras.getSelectedRow();
        txtCodigo.setText(tblPalestras.getModel().getValueAt(Codigo,0).toString());
        txtNome.setText(tblPalestras.getModel().getValueAt(Codigo,1).toString());
        txtData.setText(tblPalestras.getModel().getValueAt(Codigo,2).toString());
        String evento = tblPalestras.getModel().getValueAt(Codigo,3).toString();
        for(int i = 0; i < cbEventos.getItemCount(); i++){
            if(evento.equals(cbEventos.getItemAt(i)))
                cbEventos.setSelectedIndex(i);
        }
    }
    
    public void CarregarDoGridPalestrantes(){
        int Linha         =  tblPalestrantes.getSelectedRow();
        CodigoPalestrante =  Integer.parseInt(tblPalestrantes.getModel().getValueAt(Linha,0).toString());
        String Nome   =  tblPalestrantes.getModel().getValueAt(Linha,1).toString();
        for(int i = 1; i < cbPalestrante.getItemCount();i++){
            if(cbPalestrante.getItemAt(i).equals(Nome))
                cbPalestrante.setSelectedItem(Nome);
        }
    }
    
    public void BuscarPalestraPorNome(String parametro){
        tblPalestras.setModel(DbUtils.resultSetToTableModel(new PalestraDaoJDBC().BuscarPorNome(parametro)));
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel2 = new javax.swing.JPanel();
        txtPesquisar = new javax.swing.JTextField();
        lbBuscar = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblPalestras = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        btnDeletar = new javax.swing.JButton();
        btnLimpar = new javax.swing.JButton();
        btnSalvar = new javax.swing.JButton();
        txtCodigo = new javax.swing.JTextField();
        btnCancelar = new javax.swing.JButton();
        txtData = new javax.swing.JFormattedTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtNome = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        cbEventos = new javax.swing.JComboBox();
        jButton1 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblPalestrantes = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        btnExcluirPalestrante = new javax.swing.JButton();
        btnAdicionarPalestrante = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        cbPalestrante = new javax.swing.JComboBox();

        setTitle("Talk Me - Palestra");

        jTabbedPane1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        txtPesquisar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPesquisarKeyReleased(evt);
            }
        });

        lbBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/zoom.png"))); // NOI18N
        lbBuscar.setText("Buscar");
        lbBuscar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        tblPalestras.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblPalestras.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tblPalestras.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblPalestrasMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblPalestras);

        btnDeletar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/delete.png"))); // NOI18N
        btnDeletar.setText("Excluir");
        btnDeletar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnDeletar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeletarActionPerformed(evt);
            }
        });

        btnLimpar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/textfield_rename.png"))); // NOI18N
        btnLimpar.setText("Limpar");
        btnLimpar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnLimpar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimparActionPerformed(evt);
            }
        });

        btnSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/disk.png"))); // NOI18N
        btnSalvar.setText("Salvar");
        btnSalvar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });

        txtCodigo.setBackground(new java.awt.Color(153, 153, 153));
        txtCodigo.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        txtCodigo.setForeground(new java.awt.Color(255, 255, 255));

        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/door_in.png"))); // NOI18N
        btnCancelar.setText("Sair");
        btnCancelar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        try {
            txtData.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel1.setText("Codigo:");

        jLabel2.setText("Nome:");

        jLabel3.setText("Data:");

        jLabel4.setText("Evento:");

        cbEventos.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selecione" }));

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/information.png"))); // NOI18N
        jButton1.setText("Iniciar Votação");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnCancelar)
                        .addGap(10, 10, 10)
                        .addComponent(btnDeletar)
                        .addGap(10, 10, 10)
                        .addComponent(btnLimpar)
                        .addGap(10, 10, 10)
                        .addComponent(btnSalvar)
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addGap(18, 18, 18)
                                .addComponent(cbEventos, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(18, 18, 18)
                                .addComponent(txtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, 277, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtData, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(txtData, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbEventos, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 80, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnDeletar, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnLimpar, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(txtPesquisar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lbBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19))
        );

        jTabbedPane1.addTab("Palestra", jPanel2);

        tblPalestrantes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblPalestrantes.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tblPalestrantes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblPalestrantesMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tblPalestrantes);

        btnExcluirPalestrante.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/delete.png"))); // NOI18N
        btnExcluirPalestrante.setText("Excluir");
        btnExcluirPalestrante.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnExcluirPalestrante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirPalestranteActionPerformed(evt);
            }
        });

        btnAdicionarPalestrante.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/add.png"))); // NOI18N
        btnAdicionarPalestrante.setText("Adicionar");
        btnAdicionarPalestrante.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAdicionarPalestrante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAdicionarPalestranteActionPerformed(evt);
            }
        });

        jLabel8.setText("Palestrante");

        cbPalestrante.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selecione" }));

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cbPalestrante, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(btnAdicionarPalestrante)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnExcluirPalestrante)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbPalestrante, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8)
                    .addComponent(btnAdicionarPalestrante, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnExcluirPalestrante, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(36, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 659, Short.MAX_VALUE)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 266, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19))
        );

        jTabbedPane1.addTab("Palestrantes", jPanel3);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtPesquisarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPesquisarKeyReleased
        BuscarPalestraPorNome(txtPesquisar.getText());
        LimparCampos();
    }//GEN-LAST:event_txtPesquisarKeyReleased

    private void tblPalestrasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblPalestrasMouseClicked
       CarregarDoGrid();
       ListarPalestrantes();
    }//GEN-LAST:event_tblPalestrasMouseClicked

    private void btnDeletarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeletarActionPerformed
        try
        {
            if(txtCodigo.getText().isEmpty())
                throw new Exception("Erro: Necessário selecionar um registro!");

            if(JOptionPane.showConfirmDialog(null, "Realmente deseja excluir?","Informe",JOptionPane.WARNING_MESSAGE) == JOptionPane.YES_NO_OPTION){
               PalestraDaoJDBC palestra = new PalestraDaoJDBC();
               palestra.Deletar(Integer.parseInt(txtCodigo.getText()));
               JOptionPane.showMessageDialog(null, "Excluido com sucesso!", "Infome", JOptionPane.INFORMATION_MESSAGE);
               LimparCampos();
               ListarPalestras();
            }
        }catch(Exception ex){
                JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnDeletarActionPerformed

    private void btnLimparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimparActionPerformed
        LimparCampos();
        ListarPalestrantes();
    }//GEN-LAST:event_btnLimparActionPerformed

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
      try{   
            Palestra palestra = new Palestra();
            if(!txtCodigo.getText().isEmpty())
                palestra.setCodigo(Integer.parseInt(txtCodigo.getText()));
            
            if(!txtNome.getText().isEmpty())
                palestra.setNome(txtNome.getText().toLowerCase());
            else
                throw new Exception("Erro: Necessário informar um Nome!");
            
            if(!txtData.getText().toLowerCase().isEmpty())
                palestra.setData(new Palestra().DDMMYYYYparaYYYYMMDD(txtData.getText().toLowerCase()));
            else
                throw new Exception("Erro: Necessário informar uma data valida!");
            
            if(!cbEventos.getSelectedItem().equals(0))
                palestra.setEvento(cbEventos.getSelectedIndex());
            else
                throw new Exception("Erro: Necessário selecionar um registro!");

            palestra.Validar();

            PalestraDaoJDBC PalestraDAO = new PalestraDaoJDBC();
            PalestraDAO.Salvar(palestra);
            LimparCampos();
            ListarPalestras();

        }catch (Exception ex){
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnSalvarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        this.principal.controlabotoes(true);
        this.dispose();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void tblPalestrantesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblPalestrantesMouseClicked
       CarregarDoGridPalestrantes();
    }//GEN-LAST:event_tblPalestrantesMouseClicked

    private void btnExcluirPalestranteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirPalestranteActionPerformed
        PalestraDaoJDBC Palestra = new PalestraDaoJDBC();
        try{
            if(CodigoPalestrante < 1)
                throw new Exception("Erro: Excluir!");
            else{
                 if(JOptionPane.showConfirmDialog(null, "Realmente deseja excluir?","Informe",JOptionPane.WARNING_MESSAGE) == JOptionPane.YES_NO_OPTION)
                 {
                    Palestra.RemoverPalestrante(CodigoPalestrante);
                    ListarPalestrantes();
                    CodigoPalestrante = -1;
                 }
            }
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnExcluirPalestranteActionPerformed

    private void btnAdicionarPalestranteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAdicionarPalestranteActionPerformed
        PalestraDaoJDBC Palestra = new PalestraDaoJDBC();
        int palestranteId = -1;
        try{
            if(txtCodigo.getText().isEmpty())
                throw new Exception("Erro: Necessário selecionar uma palestra.\n "
                                   +"Guia Palestra!");
            if(cbPalestrante.getSelectedIndex() == 0)
                throw new Exception("Erro: Necessário selecionar um palestrante!");
            palestranteId = Palestra.BuscarIdPorNome(cbPalestrante.getSelectedItem().toString());
            
            if(palestranteId < 1)
                throw new Exception("Erro: Ao buscar palestrante!");
            else
                Palestra.AdicionarPalestrante(Integer.parseInt(txtCodigo.getText()), palestranteId);
            
            ListarPalestrantes();
        }catch(Exception ex ){
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnAdicionarPalestranteActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if(!txtCodigo.getText().isEmpty()){
            new JFrmVotacao(principal,Integer.parseInt(txtCodigo.getText()), this.usuarioLogado, txtNome.getText()).setVisible(true);
        }else{
           JOptionPane.showMessageDialog(null, "Erro: Necessário ter uma palestra selecionada!", "Erro", JOptionPane.ERROR_MESSAGE); 
        }
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdicionarPalestrante;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnDeletar;
    private javax.swing.JButton btnExcluirPalestrante;
    private javax.swing.JButton btnLimpar;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JComboBox cbEventos;
    private javax.swing.JComboBox cbPalestrante;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel lbBuscar;
    private javax.swing.JTable tblPalestrantes;
    private javax.swing.JTable tblPalestras;
    private javax.swing.JTextField txtCodigo;
    private javax.swing.JFormattedTextField txtData;
    private javax.swing.JTextField txtNome;
    private javax.swing.JTextField txtPesquisar;
    // End of variables declaration//GEN-END:variables
}
