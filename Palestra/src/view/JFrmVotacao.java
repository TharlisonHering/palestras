package view;

import JDBC.ResultadoDaoJDBC;
import JDBC.UsuarioDaoJDBC;
import JDBC.VotosDaoJDBC;
import Model.Usuario;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class JFrmVotacao extends javax.swing.JFrame {
    JFrmPrincipal princiapl;
    int PalestraID = -1 ;
    Usuario usuarioLogado;
    
    public JFrmVotacao(JFrmPrincipal pai, int palestraid, Usuario usuario, String nomeVotacao) {
        initComponents();
        
        this.setLocationRelativeTo(null);
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.setIconImage(new ImageIcon(getClass().getResource("/images/icon-frames.png")).getImage());
        this.princiapl = pai;
        this.PalestraID = palestraid;
        this.usuarioLogado = usuario;
        princiapl.setVisible(false);
        this.txtLabelNota.setText(this.txtLabelNota.getText() +"  -  "+ nomeVotacao);
        
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        panelBotoes = new javax.swing.JPanel();
        btnFinalizarVotacao = new javax.swing.JButton();
        PanelNotas = new javax.swing.JPanel();
        txtLabelNota = new javax.swing.JLabel();
        lbVoto_0 = new javax.swing.JLabel();
        lbVoto_2 = new javax.swing.JLabel();
        lbVoto_1 = new javax.swing.JLabel();
        lbVoto_3 = new javax.swing.JLabel();
        lbVoto_4 = new javax.swing.JLabel();
        lbVoto_5 = new javax.swing.JLabel();
        lbVoto_6 = new javax.swing.JLabel();
        lbVoto_7 = new javax.swing.JLabel();
        lbVoto_8 = new javax.swing.JLabel();
        lbVoto_9 = new javax.swing.JLabel();
        lbVoto_10 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Talk Me - Votação");

        btnFinalizarVotacao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/script.png"))); // NOI18N
        btnFinalizarVotacao.setText("Encerrar");
        btnFinalizarVotacao.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnFinalizarVotacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFinalizarVotacaoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelBotoesLayout = new javax.swing.GroupLayout(panelBotoes);
        panelBotoes.setLayout(panelBotoesLayout);
        panelBotoesLayout.setHorizontalGroup(
            panelBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelBotoesLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnFinalizarVotacao, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19))
        );
        panelBotoesLayout.setVerticalGroup(
            panelBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelBotoesLayout.createSequentialGroup()
                .addContainerGap(22, Short.MAX_VALUE)
                .addComponent(btnFinalizarVotacao, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        PanelNotas.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        PanelNotas.setLayout(new java.awt.GridBagLayout());

        txtLabelNota.setFont(new java.awt.Font("Tahoma", 0, 50)); // NOI18N
        txtLabelNota.setText("Deixe sua nota");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 14;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 20, 15, 5);
        PanelNotas.add(txtLabelNota, gridBagConstraints);

        lbVoto_0.setIcon(new javax.swing.ImageIcon(getClass().getResource("/NewImagensRedimencionadas/icons8-0-80.png"))); // NOI18N
        lbVoto_0.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbVoto_0.setPreferredSize(new java.awt.Dimension(120, 110));
        lbVoto_0.setRequestFocusEnabled(false);
        lbVoto_0.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbVoto_0MouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        PanelNotas.add(lbVoto_0, gridBagConstraints);

        lbVoto_2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/NewImagensRedimencionadas/icon-2-80px.png"))); // NOI18N
        lbVoto_2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbVoto_2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbVoto_2MouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        PanelNotas.add(lbVoto_2, gridBagConstraints);

        lbVoto_1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/NewImagensRedimencionadas/icon-1-80px.png"))); // NOI18N
        lbVoto_1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbVoto_1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbVoto_1MouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        PanelNotas.add(lbVoto_1, gridBagConstraints);

        lbVoto_3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/NewImagensRedimencionadas/icon-3-80px.png"))); // NOI18N
        lbVoto_3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbVoto_3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbVoto_3MouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        PanelNotas.add(lbVoto_3, gridBagConstraints);

        lbVoto_4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/NewImagensRedimencionadas/icon-4-80px.png"))); // NOI18N
        lbVoto_4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbVoto_4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbVoto_4MouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        PanelNotas.add(lbVoto_4, gridBagConstraints);

        lbVoto_5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/NewImagensRedimencionadas/icon-5-80px.png"))); // NOI18N
        lbVoto_5.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbVoto_5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbVoto_5MouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        PanelNotas.add(lbVoto_5, gridBagConstraints);

        lbVoto_6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/NewImagensRedimencionadas/icon-6-80px.png"))); // NOI18N
        lbVoto_6.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbVoto_6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbVoto_6MouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 9;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        PanelNotas.add(lbVoto_6, gridBagConstraints);

        lbVoto_7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/NewImagensRedimencionadas/icon-7-80px.png"))); // NOI18N
        lbVoto_7.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbVoto_7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbVoto_7MouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        PanelNotas.add(lbVoto_7, gridBagConstraints);

        lbVoto_8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/NewImagensRedimencionadas/icon-8-80px.png"))); // NOI18N
        lbVoto_8.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbVoto_8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbVoto_8MouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 11;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        PanelNotas.add(lbVoto_8, gridBagConstraints);

        lbVoto_9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/NewImagensRedimencionadas/icon-9-80px.png"))); // NOI18N
        lbVoto_9.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbVoto_9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbVoto_9MouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        PanelNotas.add(lbVoto_9, gridBagConstraints);

        lbVoto_10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/NewImagensRedimencionadas/icon-10-80px.png"))); // NOI18N
        lbVoto_10.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbVoto_10.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbVoto_10MouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 13;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        PanelNotas.add(lbVoto_10, gridBagConstraints);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelBotoes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(PanelNotas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(PanelNotas, javax.swing.GroupLayout.DEFAULT_SIZE, 210, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelBotoes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnFinalizarVotacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFinalizarVotacaoActionPerformed
        String senha = JOptionPane.showInputDialog(this, "Senha de administrador");
        if(!senha.isEmpty()){   
            UsuarioDaoJDBC user = new UsuarioDaoJDBC();
            if(!user.ValidarUsuario(this.usuarioLogado.getNome(), senha.toLowerCase())){
                JOptionPane.showMessageDialog(null, "Senha inválida, fornceça uma credencial valida!");
            }else{
                new ResultadoDaoJDBC().Calcular(PalestraID);
                princiapl.setVisible(true);
                this.dispose();
            }
        }else
            JOptionPane.showMessageDialog(null, "Senha inválida, fornceça uma credencial valida!");
    }//GEN-LAST:event_btnFinalizarVotacaoActionPerformed

    private void lbVoto_0MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbVoto_0MouseClicked
        new VotosDaoJDBC().Votar(0, PalestraID);
    }//GEN-LAST:event_lbVoto_0MouseClicked

    private void lbVoto_1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbVoto_1MouseClicked
       new VotosDaoJDBC().Votar(1, PalestraID);
    }//GEN-LAST:event_lbVoto_1MouseClicked

    private void lbVoto_2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbVoto_2MouseClicked
        new VotosDaoJDBC().Votar(2, PalestraID);
    }//GEN-LAST:event_lbVoto_2MouseClicked

    private void lbVoto_3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbVoto_3MouseClicked
        new VotosDaoJDBC().Votar(3, PalestraID);
    }//GEN-LAST:event_lbVoto_3MouseClicked

    private void lbVoto_4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbVoto_4MouseClicked
       new VotosDaoJDBC().Votar(4, PalestraID);
    }//GEN-LAST:event_lbVoto_4MouseClicked

    private void lbVoto_5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbVoto_5MouseClicked
       new VotosDaoJDBC().Votar(5, PalestraID);
    }//GEN-LAST:event_lbVoto_5MouseClicked

    private void lbVoto_6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbVoto_6MouseClicked
        new VotosDaoJDBC().Votar(6, PalestraID);
    }//GEN-LAST:event_lbVoto_6MouseClicked

    private void lbVoto_7MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbVoto_7MouseClicked
        new VotosDaoJDBC().Votar(7, PalestraID);
    }//GEN-LAST:event_lbVoto_7MouseClicked

    private void lbVoto_8MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbVoto_8MouseClicked
        new VotosDaoJDBC().Votar(8, PalestraID);
    }//GEN-LAST:event_lbVoto_8MouseClicked

    private void lbVoto_9MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbVoto_9MouseClicked
       new VotosDaoJDBC().Votar(9, PalestraID);
    }//GEN-LAST:event_lbVoto_9MouseClicked

    private void lbVoto_10MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbVoto_10MouseClicked
        new VotosDaoJDBC().Votar(10, PalestraID);
    }//GEN-LAST:event_lbVoto_10MouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel PanelNotas;
    private javax.swing.JButton btnFinalizarVotacao;
    private javax.swing.JLabel lbVoto_0;
    private javax.swing.JLabel lbVoto_1;
    private javax.swing.JLabel lbVoto_10;
    private javax.swing.JLabel lbVoto_2;
    private javax.swing.JLabel lbVoto_3;
    private javax.swing.JLabel lbVoto_4;
    private javax.swing.JLabel lbVoto_5;
    private javax.swing.JLabel lbVoto_6;
    private javax.swing.JLabel lbVoto_7;
    private javax.swing.JLabel lbVoto_8;
    private javax.swing.JLabel lbVoto_9;
    private javax.swing.JPanel panelBotoes;
    private javax.swing.JLabel txtLabelNota;
    // End of variables declaration//GEN-END:variables
}
