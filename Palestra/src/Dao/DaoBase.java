package Dao;

import java.sql.ResultSet;

public abstract interface DaoBase {
	public ResultSet Listar();
        public ResultSet BuscarPorNome(String nome);
        public void Deletar(int codigo);
}
