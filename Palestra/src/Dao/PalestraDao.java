package Dao;

import Model.Palestra;

public interface PalestraDao extends DaoBase{
    
    public void Salvar(Palestra palestra);
    
}
