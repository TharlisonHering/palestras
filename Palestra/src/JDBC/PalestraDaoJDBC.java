package JDBC;

import Dao.PalestraDao;
import Model.Palestra;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class PalestraDaoJDBC implements PalestraDao{
    String _sql;
    
    @Override
    public void Salvar(Palestra palestra) 
    {
        if(palestra.Codigo == -1)
        {
            try 
            {
                 Connection conn = JDBCUtil.getConexao();
                _sql = "INSERT INTO PALESTRAS (NOME,DATA,EVENTO) VALUES (?,?,?);";	

                PreparedStatement Insere = (PreparedStatement) conn.prepareStatement(_sql); 
                Insere.setString(1,palestra.getNome());
                Insere.setString(2,palestra.getData());
                Insere.setInt(3,palestra.getEvento());
                Insere.execute();
                
                JDBCUtil.CloseConexao(conn);
                JOptionPane.showMessageDialog(null,"Salvo com sucesso!","Informe",JOptionPane.INFORMATION_MESSAGE);
            } catch (SQLException e) 
            {
                JOptionPane.showMessageDialog(null, "Erro ao cadastrar:" + e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            }
        }else
        {
            try
            {
                Connection conn = JDBCUtil.getConexao();
                _sql = "UPDATE EVENTOS SET NOME = ? WHERE CODIGO = ? ;";	

                PreparedStatement Update = (PreparedStatement) conn.prepareStatement(_sql); 
                Update.setString(1,palestra.getNome());
                Update.setInt(2,palestra.getCodigo());
                Update.execute();
                JDBCUtil.CloseConexao(conn);
                JOptionPane.showMessageDialog(null,"Salvo com sucesso!","Informe",JOptionPane.INFORMATION_MESSAGE);
            } catch (SQLException e) 
            {
                JOptionPane.showMessageDialog(null, "Erro ao cadastrar:" + e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            }
            
        }
    }

    @Override
    public ResultSet Listar()
    {
        try 
        {
            Connection conn = JDBCUtil.getConexao();
            _sql = "SELECT A.Codigo, A.Nome, DATE_FORMAT(A.Data, \"%d/%h/%Y\") as Data, B.Nome AS Evento " +
                   "  FROM PALESTRAS A "                                +
                   "  INNER JOIN EVENTOS B ON B.CODIGO = A.EVENTO "     +
                   "  GROUP BY A.CODIGO, A.NOME, A.DATA, A.EVENTO "     +
                   " ORDER BY A.CODIGO DESC; ";	

            PreparedStatement Busca = (PreparedStatement) conn.prepareStatement(_sql); 
            return Busca.executeQuery();
        } catch (SQLException e) 
        {
            JOptionPane.showMessageDialog(null, "Erro ao Buscar:" + e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            return null;
        }	 
    }

    public int ValidarCodigo(String codigo){
        int Codigo;
        try{
            Codigo = Integer.parseInt(codigo);
        }
        catch(NumberFormatException ex){
            Codigo = 0;
        }
        return Codigo;
    }
    
    @Override
    public ResultSet BuscarPorNome(String nome)
    {
        try 
        {
            Connection conn = JDBCUtil.getConexao();
            _sql = "SELECT A.Codigo, A.Nome, DATE_FORMAT(A.Data, \"%d/%h/%Y\") as Data, B.Nome AS Evento " +
                   "  FROM PALESTRAS A "                                +
                   "  INNER JOIN EVENTOS B ON B.CODIGO = A.EVENTO "     +
                    " WHERE A.NOME LIKE ? OR A.CODIGO IN(?)       "     +
                   "  GROUP BY A.CODIGO, A.NOME, A.DATA, A.EVENTO "     +
                   " ORDER BY A.CODIGO DESC; ";	
            
            PreparedStatement Busca = (PreparedStatement) conn.prepareStatement(_sql); 
            Busca.setString(1, "%"+nome+"%");
            Busca.setInt(2,ValidarCodigo(nome));
            return Busca.executeQuery();
        } catch (SQLException e) 
        {
            JOptionPane.showMessageDialog(null, "Erro ao buscar:" + e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            return null;
        }
    }

    @Override
    public void Deletar(int codigo) 
    {
        try 
        {
            Connection conn = JDBCUtil.getConexao();
            _sql = "DELETE FROM PALESTRAS WHERE CODIGO = ?;";	
            
            PreparedStatement Delete = (PreparedStatement) conn.prepareStatement(_sql); 
            Delete.setInt(1, codigo);
            Delete.execute();
            
        } catch (SQLException e) 
        {
            JOptionPane.showMessageDialog(null, "Erro ao Excluir:" + e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
        }	 
    }
    
    public ResultSet BuscarEventos() 
    {
        try 
        {
           Connection conn = JDBCUtil.getConexao();
            _sql = "SELECT * FROM EVENTOS;";	

            PreparedStatement Busca = (PreparedStatement) conn.prepareStatement(_sql); 
            return Busca.executeQuery();
        } catch (SQLException e) 
        {
            JOptionPane.showMessageDialog(null, "Erro ao buscar Eventos:" + e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            return null;
        }	 
    }
    
    public ResultSet BuscarPalestrantes() 
    {
        try 
        {
           Connection conn = JDBCUtil.getConexao();
            _sql = "SELECT * FROM PALESTRANTES;";	

            PreparedStatement Busca = (PreparedStatement) conn.prepareStatement(_sql); 
            return Busca.executeQuery();
        } catch (SQLException e) 
        {
            JOptionPane.showMessageDialog(null, "Erro ao buscar Palestrantes:" + e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            return null;
        }	 
    }
    
    public void AdicionarPalestrante(int Palestra, int Palestrante)
    {
        try 
            {
                int count = -1;
                 Connection conn = JDBCUtil.getConexao();
                 _sql = " SELECT COUNT(CODIGO)QTD                                  " +
                        "   FROM PALESTRANTEPALESTRAS                              " +
                        "  WHERE PALESTRA = ? AND PALESTRANTE = ?;                 ";
                
                PreparedStatement Busca = (PreparedStatement) conn.prepareStatement(_sql); 
                Busca.setInt(1,Palestra);
                Busca.setInt(2,Palestrante);
                ResultSet rs = Busca.executeQuery();
                while(rs.next())
                    count = rs.getInt("QTD");
                if(count > 0)
                    throw new Exception("Erro: Palestrante já cadastrado nessa palestra! \n");
                
                 
                _sql = "INSERT INTO PALESTRANTEPALESTRAS (PALESTRANTE,PALESTRA) VALUES (?,?);";	

                PreparedStatement Insere = (PreparedStatement) conn.prepareStatement(_sql); 
                Insere.setInt(1,Palestrante);
                Insere.setInt(2,Palestra);
                Insere.execute();
                JDBCUtil.CloseConexao(conn);
                
            }catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "Erro ao cadastrar:" + e.getMessage() + "\n", "Erro", JOptionPane.ERROR_MESSAGE);
            }catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Erro ao cadastrar:" + ex.getMessage() + "\n", "Erro", JOptionPane.ERROR_MESSAGE);
            }
    }
   
    public void RemoverPalestrante(int codigo)
    {
        try 
        {
            Connection conn = JDBCUtil.getConexao();
            _sql = "DELETE FROM PALESTRANTEPALESTRAS WHERE CODIGO = ?;";	
            
            PreparedStatement Delete = (PreparedStatement) conn.prepareStatement(_sql); 
            Delete.setInt(1, codigo);
            Delete.execute();
            
        } catch (SQLException e) 
        {
            JOptionPane.showMessageDialog(null, "Erro ao Excluir:" + e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }

    public int BuscarIdPorNome(String nome)
    {
        try 
        {
            int codigo = -1;
            Connection conn = JDBCUtil.getConexao();
            _sql = "SELECT CODIGO FROM PALESTRANTES WHERE NOME = ? ";	
            
            PreparedStatement Busca = (PreparedStatement) conn.prepareStatement(_sql); 
            Busca.setString(1,nome);
            ResultSet rs = Busca.executeQuery();
            while(rs.next())
                codigo = rs.getInt("Codigo");
            
            return codigo;
        } catch (SQLException e) 
        {
            JOptionPane.showMessageDialog(null, "Erro ao buscar:" + e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            return -1;
        }
    }

    public ResultSet BuscarPalestrantesDaPalestra(int codigo)
    {
        try 
        {
            Connection conn = JDBCUtil.getConexao();
            if(codigo >= 1){
                _sql = " SELECT A.Codigo, B.NOME AS Palestrante                    " +
                        "   FROM PALESTRANTEPALESTRAS A                             " +
                        "  INNER JOIN PALESTRANTES    B ON B.CODIGO = A.PALESTRANTE " +
                        " WHERE A.PALESTRA = ? ;                                    ";	

                 PreparedStatement Busca = (PreparedStatement) conn.prepareStatement(_sql);
                 Busca.setInt(1, codigo);
                 return Busca.executeQuery();
            }else{
                 _sql = " SELECT A.Codigo, B.NOME AS Palestrante                    " +
                        "   FROM PALESTRANTEPALESTRAS A                             " +
                        "  INNER JOIN PALESTRANTES    B ON B.CODIGO = A.PALESTRANTE " +
                        " WHERE A.CODIGO = -1 ;                                     ";	

                 PreparedStatement Busca = (PreparedStatement) conn.prepareStatement(_sql);
                 return Busca.executeQuery();

            }
        }catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro ao buscar Palestrantes:" + e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            return null;
        }	 
    }

    public ResultSet BuscarPalestraPorEvento(int evento)
    {
        try 
        {
            String where ="";
            Connection conn = JDBCUtil.getConexao();
            _sql = "SELECT * FROM PALESTRAS ";	
            if(evento > 0){
                where = "WHERE EVENTO = ?";
                _sql += where;
            }    
            PreparedStatement Busca = (PreparedStatement) conn.prepareStatement(_sql); 
            if(evento > 0)
                Busca.setInt(1,evento);
            
            return Busca.executeQuery();
        } catch (SQLException e) 
        {
            JOptionPane.showMessageDialog(null, "Erro ao buscar:" + e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            return null;
        }
        
    }
}
