package JDBC;

import com.itextpdf.text.Document;
import javax.swing.JOptionPane;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Color;
import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class RelatorioDaoJDBC {
    
    public void GerarPdf(int Evento, int Palestra){
        Document doc = new Document();
        String _sql;
        try{
            
            Connection conn = JDBCUtil.getConexao();
            _sql = " SELECT B.NOME, B.DATA, C.TOTALVOTOS, C.RESULTADO " +
                    "   FROM EVENTOS A " +
                    "  INNER JOIN PALESTRAS B ON B.EVENTO = A.CODIGO   " +
                    "  INNER JOIN RESULTADO C ON C.PALESTRA = B.CODIGO ";
            
            String where = "";
            int parametros = 0;
            
            if(Evento > 0){
                where += " WHERE A.CODIGO = ?";
                parametros++;            
                
                if(Palestra > 0){
                    parametros++;
                    where += "   AND B.CODIGO = ?";
                }
            }
            _sql += where;
            
            PreparedStatement Insere = (PreparedStatement) conn.prepareStatement(_sql); 
            if(parametros > 1){
                Insere.setInt(1,Evento);
                Insere.setInt(2,Palestra);
            }
            else if(parametros > 0)
                Insere.setInt(1,Evento);
            
            
            ResultSet rs = Insere.executeQuery();

            PdfWriter.getInstance(doc, new FileOutputStream("Relatorio.pdf"));
            doc.open();
            doc.setPageSize(PageSize.A4);
            
            Paragraph p = new Paragraph("Talk Me - Relatorio de qualidade das Palestras");
            p.setAlignment(1);
            
            doc.add(p);
            p = new Paragraph(" ");
            p.setAlignment(1);
            doc.add(p);
            p = new Paragraph(" ");
            p.setAlignment(1);
            doc.add(p);
            p = new Paragraph("");
            
            PdfPTable table = new PdfPTable(4);
            
            PdfPCell NomePalestra = new PdfPCell(new Paragraph("Nome"));
            PdfPCell QntVotos     = new PdfPCell(new Paragraph("Numero de Votos"));
            PdfPCell Nota         = new PdfPCell(new Paragraph("Percentual de satisfação"));
            PdfPCell Data         = new PdfPCell(new Paragraph("Data da palestra"));
            
            table.addCell(NomePalestra);
            table.addCell(QntVotos);
            table.addCell(Nota);
            table.addCell(Data);
        
            while(rs.next()){
                NomePalestra = new PdfPCell(new Paragraph(rs.getString("NOME")));
                QntVotos     = new PdfPCell(new Paragraph(rs.getString("TOTALVOTOS")));
                Nota         = new PdfPCell(new Paragraph(rs.getString("RESULTADO")+"%"));
                Data         = new PdfPCell(new Paragraph(rs.getString("DATA")));
                
                table.addCell(NomePalestra);
                table.addCell(QntVotos);
                table.addCell(Nota);
                table.addCell(Data);
            }
            doc.add(table);
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "Erro" + ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
        }finally{
            doc.close();
        }
        
        try{
            Desktop.getDesktop().open( new File("Relatorio.pdf"));
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "Erro, relatorio não encontrado:" + ex.getMessage());
        }
    }    
}












