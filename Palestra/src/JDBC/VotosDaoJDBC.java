package JDBC;

import Dao.VotosDao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class VotosDaoJDBC implements VotosDao{
    String _sql;
    
    @Override
    public void Votar(int nota, int palestra)
    {
        try
        {
               Connection conn = JDBCUtil.getConexao();
               _sql = "INSERT INTO VOTOS(PALESTRA, NOTA) VALUES(?,?);";	
            
               PreparedStatement Insere = (PreparedStatement) conn.prepareStatement(_sql); 
               Insere.setInt(1,palestra);
               Insere.setInt(2,nota);
               Insere.execute();
               JDBCUtil.CloseConexao(conn);
               JOptionPane.showMessageDialog(null,"Nota salva !","Informe",JOptionPane.INFORMATION_MESSAGE);
        }catch (SQLException e){
               JOptionPane.showMessageDialog(null, "Erro ao salvar:" + e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
        }
     
    }
    
}
