package JDBC;

import Dao.UsuarioDao;
import Model.Usuario;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

public class UsuarioDaoJDBC implements UsuarioDao{
    String _sql;
    
    @Override
    public void Salvar(Usuario evento) 
    {
        throw new UnsupportedOperationException("Não Implementado."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ResultSet Listar() {
        throw new UnsupportedOperationException("Não Implementado."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ResultSet BuscarPorNome(String nome) {
        throw new UnsupportedOperationException("Não Implementado."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void Deletar(int codigo) {
        throw new UnsupportedOperationException("Não Implementado."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public boolean ValidarUsuario(Usuario user)
    {
        try 
        {
            Connection conn = JDBCUtil.getConexao();
            _sql = "SELECT * FROM USUARIOS WHERE NOME = '"+user.getNome()+"' AND SENHA = '"+user.getSenha()+"'";	
            Statement Busca = conn.createStatement();
            ResultSet rs = Busca.executeQuery(_sql);
            if(rs.next()){
                return rs.getInt("Codigo") > 0;
            }else
                return false;
        } catch (SQLException e) 
        {
            JOptionPane.showMessageDialog(null, "Erro ao validar usuario" + e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }
    
    public boolean ValidarUsuario(String nome, String senha)
    {
        try 
        {
            Connection conn = JDBCUtil.getConexao();
            _sql = "SELECT * FROM USUARIOS WHERE NOME = '"+nome+"' AND SENHA = '"+senha+"'";	
            Statement Busca = conn.createStatement();
            ResultSet rs = Busca.executeQuery(_sql);
            if(rs.next()){
                return rs.getInt("Codigo") > 0;
            }else
                return false;
        } catch (SQLException e) 
        {
            JOptionPane.showMessageDialog(null, "Erro ao validar usuario" + e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }
}
