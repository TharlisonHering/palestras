package JDBC;

import Dao.PalestranteDao;
import Model.Palestrante;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class PalestranteDaoJDBC implements PalestranteDao{
    
    String _sql;
    
    @Override
    public void Salvar(Palestrante palestrante) 
    {
        if(palestrante.Codigo == -1)
        {
            try 
            {
                Connection conn = JDBCUtil.getConexao();
                _sql = "INSERT INTO PALESTRANTES (NOME,EMAIL,TELEFONE,DESCRICAO) VALUES (?,?,?,?);";	

                PreparedStatement Insere = (PreparedStatement) conn.prepareStatement(_sql); 
                Insere.setString(1,palestrante.getNome());
                Insere.setString(2,palestrante.getEmail());
                Insere.setString(3,palestrante.getTelefone());
                Insere.setString(4,palestrante.getDescricao());
                Insere.execute();
                
                JDBCUtil.CloseConexao(conn);
                JOptionPane.showMessageDialog(null,"Salvo com sucesso!","Informe",JOptionPane.INFORMATION_MESSAGE);
            } catch (SQLException e) 
            {
                JOptionPane.showMessageDialog(null, "Erro ao cadastrar:" + e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            }
        }else
        {
            try
            {
                Connection conn = JDBCUtil.getConexao();
                _sql = "UPDATE PALESTRANTES SET NOME = ?, EMAIL = ?, TELEFONE = ?, DESCRICAO = ? WHERE CODIGO = ? ;";	

                PreparedStatement Update = (PreparedStatement) conn.prepareStatement(_sql); 
                Update.setString(1,palestrante.getNome());
                Update.setString(2,palestrante.getEmail());
                Update.setString(3,palestrante.getTelefone());
                Update.setString(4,palestrante.getDescricao());
                Update.setInt(5,palestrante.getCodigo());
                Update.execute();
                
                JDBCUtil.CloseConexao(conn);
                JOptionPane.showMessageDialog(null,"Salvo com sucesso!","Informe",JOptionPane.INFORMATION_MESSAGE);
            } catch (SQLException e) 
            {
                JOptionPane.showMessageDialog(null, "Erro ao cadastrar:" + e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            }   
        }
    }

    @Override
    public ResultSet Listar() 
    {
         try 
        {
            Connection conn = JDBCUtil.getConexao();
            _sql = "SELECT * FROM PALESTRANTES ORDER BY CODIGO DESC;";	

            PreparedStatement Busca = (PreparedStatement) conn.prepareStatement(_sql); 
            return Busca.executeQuery();
        } catch (SQLException e) 
        {
            JOptionPane.showMessageDialog(null, "Erro ao buscar:" + e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            return null;
        }	 
    }

    public int ValidarCodigo(String codigo){
        int Codigo;
        try{
            Codigo = Integer.parseInt(codigo);
        }
        catch(NumberFormatException ex){
            Codigo = 0;
        }
        return Codigo;
    }
    
    @Override
    public ResultSet BuscarPorNome(String nome) 
    {
        try 
        {
            Connection conn = JDBCUtil.getConexao();
            _sql = "SELECT * FROM PALESTRANTES WHERE NOME LIKE ? OR CODIGO IN(?);";	

            PreparedStatement Busca = (PreparedStatement) conn.prepareStatement(_sql); 
            Busca.setString(1, "%"+nome+"%");
            Busca.setInt(2,ValidarCodigo(nome));
            return Busca.executeQuery();
        } catch (SQLException e) 
        {
            JOptionPane.showMessageDialog(null, "Erro ao buscar:" + e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            return null;
        }
    }

    @Override
    public void Deletar(int codigo) 
    {
     try 
        {
            Connection conn = JDBCUtil.getConexao();
            _sql = "DELETE FROM PALESTRANTES WHERE CODIGO = ?;";	
            
            PreparedStatement Delete = (PreparedStatement) conn.prepareStatement(_sql); 
            Delete.setInt(1, codigo);
            Delete.execute();
            
        } catch (SQLException e) 
        {
            JOptionPane.showMessageDialog(null, "Erro ao Excluir:" + e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
        }	 
    }
    
}
