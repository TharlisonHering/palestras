package JDBC;

import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import javax.swing.JOptionPane;

public class ResultadoDaoJDBC {
    String _sql;
    
    public void ExcluirCalculoExistente(int IdPalestra)
    {
        
        try 
        {
            Connection conn = JDBCUtil.getConexao();
            _sql = "DELETE FROM RESULTADO WHERE PALESTRA = ?;";	
            
            PreparedStatement Delete = (PreparedStatement) conn.prepareStatement(_sql); 
            Delete.setInt(1, IdPalestra);
            Delete.execute();
            
        } catch (SQLException e) 
        {
            JOptionPane.showMessageDialog(null, "Erro ao excluir calculo existente :" + e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
        }
        
    }
    public void Calcular(int IdPalestra)
    {
        try 
        {
            ExcluirCalculoExistente(IdPalestra);
            float Promotores = 0, Detratores = 0, Neutros = 0, Total = 0, Resultado = 0;
            DecimalFormat decimal = new DecimalFormat( "0.00" );  
            
            Connection conn = JDBCUtil.getConexao();
            
            _sql = "SELECT * FROM VOTOS WHERE PALESTRA = ?";

            PreparedStatement Busca = (PreparedStatement) conn.prepareStatement(_sql); 
            Busca.setInt(1,IdPalestra);
            ResultSet rs = Busca.executeQuery();

            while(rs.next())
            {
                if(rs.getInt("NOTA") < 7){
                    Detratores += 1;
                }else if(rs.getInt("NOTA") < 9){
                    Neutros += 1;
                }else
                    Promotores += 1;
            }
            
            Total = Detratores + Neutros + Promotores;
            Resultado = ( ((Promotores / Total) - (Detratores / Total)) * 100);
            
             _sql = "INSERT INTO RESULTADO (PALESTRA, RESULTADO, TOTALVOTOS) VALUES(?,?,?);";
            PreparedStatement Insere = (PreparedStatement) conn.prepareStatement(_sql); 
            Insere.setInt(1,IdPalestra);
            Insere.setFloat(2,Resultado);
            Insere.setFloat(3,Total);
            Insere.execute();

        }catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro ao cadastrar:" + e.getMessage() + "\n", "Erro", JOptionPane.ERROR_MESSAGE);
        }catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Erro ao cadastrar:" + ex.getMessage() + "\n", "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }
}
