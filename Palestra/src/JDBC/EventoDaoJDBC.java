package JDBC;
import Dao.EventoDao;
import Model.Evento;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class EventoDaoJDBC implements EventoDao{
    String _sql;
    
    @Override
    public void Salvar(Evento evento) 
    {
        if(evento.Codigo == -1)
        {
            try 
            {
                Connection conn = JDBCUtil.getConexao();
                _sql = "INSERT INTO EVENTOS (NOME) VALUES (?);";	

                PreparedStatement Insere = (PreparedStatement) conn.prepareStatement(_sql); 
                Insere.setString(1,evento.getNome());
                Insere.execute();
                JDBCUtil.CloseConexao(conn);
                JOptionPane.showMessageDialog(null,"Salvo com sucesso!","Informe",JOptionPane.INFORMATION_MESSAGE);
            } catch (SQLException e) 
            {
                JOptionPane.showMessageDialog(null, "Erro ao cadastrar:" + e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            }
        }else
        {
            try
            {
                Connection conn = JDBCUtil.getConexao();
                _sql = "UPDATE EVENTOS SET NOME = ? WHERE CODIGO = ? ;";	

                PreparedStatement Update = (PreparedStatement) conn.prepareStatement(_sql); 
                Update.setString(1,evento.getNome());
                Update.setInt(2,evento.getCodigo());
                Update.execute();
                JDBCUtil.CloseConexao(conn);
                JOptionPane.showMessageDialog(null,"Salvo com sucesso!","Informe",JOptionPane.INFORMATION_MESSAGE);
            } catch (SQLException e) 
            {
                JOptionPane.showMessageDialog(null, "Erro ao cadastrar:" + e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            }
            
        }
    }
            
    public int ValidarCodigo(String codigo){
        int Codigo;
        try{
            Codigo = Integer.parseInt(codigo);
        }
        catch(NumberFormatException ex){
            Codigo = 0;
        }
        return Codigo;
    }
    
    @Override
    public ResultSet BuscarPorNome(String nome)
    {
        try 
        {
            Connection conn = JDBCUtil.getConexao();
            _sql = "SELECT * FROM EVENTOS WHERE NOME LIKE ? OR CODIGO IN(?);";	
            
            PreparedStatement Busca = (PreparedStatement) conn.prepareStatement(_sql); 
            Busca.setString(1, "%"+nome+"%");
            Busca.setInt(2,ValidarCodigo(nome));
            return Busca.executeQuery();
        } catch (SQLException e) 
        {
            JOptionPane.showMessageDialog(null, "Erro ao buscar:" + e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            return null;
        }
    }
    
    @Override
    public ResultSet Listar()
    {
        try 
        {
            Connection conn = JDBCUtil.getConexao();
            _sql = "SELECT * FROM EVENTOS ORDER BY CODIGO DESC;";	

            PreparedStatement Busca = (PreparedStatement) conn.prepareStatement(_sql); 
            return Busca.executeQuery();
        } catch (SQLException e) 
        {
            JOptionPane.showMessageDialog(null, "Erro ao Buscar:" + e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            return null;
        }	 
    }

    @Override
    public void Deletar(int codigo) 
    {
        try 
        {
            Connection conn = JDBCUtil.getConexao();
            _sql = "DELETE FROM EVENTOS WHERE CODIGO = ?;";	
            
            PreparedStatement Delete = (PreparedStatement) conn.prepareStatement(_sql); 
            Delete.setInt(1, codigo);
            Delete.execute();
            
        } catch (SQLException e) 
        {
            JOptionPane.showMessageDialog(null, "Erro ao Excluir:" + e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
        }	 
    }
}
