package Model;

public class Usuario {
    public int Codigo;
    public String Nome;
    public String Email;
    public String Senha;
    
    public Usuario(){
        this.Codigo = -1;
    }
    
    public int getCodigo(){
        return this.Codigo;
    }
    public void setCodigo(int codigo){
        this.Codigo = codigo;
    }
    
    public String getNome(){
        return this.Nome;
    }
    public void setNome(String nome){
        this.Nome = nome;
    }
    
    public String getEmail(){
        return this.Email;
    }
    public void setEmail(String email){
        this.Email = email;
    }
    
    public String getSenha(){
        return this.Senha;
    }
    public void setSenha(String senha){
        this.Senha = senha;
    } 
}
