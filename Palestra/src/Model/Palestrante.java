package Model;
public class Palestrante {
    public int Codigo;
    public String Nome;
    public String Telefone;
    public String Email;
    public String Descricao;
    
    public Palestrante(){
        this.Codigo = -1;
    }
    
    public int getCodigo()
    {
        return this.Codigo;
    }
    public void setCodigo(int codigo)
    {
        this.Codigo = codigo;
    }
    
    public String getNome()
    {
        return this.Nome;
    }
    public void setNome(String nome)
    {
        this.Nome = nome;
    }
    
    public String getTelefone()
    {
        return this.Telefone;
    }
    public void setTelefone(String telefone)
    {
        this.Telefone = telefone;
    }
    
    public String getEmail()
    {
        return this.Email;
    }
    public void setEmail(String email)
    {
        this.Email = email;
    }
    
    public String getDescricao()
    {
        return this.Descricao;
    }
    public void setDescricao(String descricao)
    {
        this.Descricao = descricao;
    }
    
    public void validar(){
    
    }
}
