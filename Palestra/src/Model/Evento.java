package Model;
public class Evento {
    public int Codigo;
    public String Nome;
    
    public Evento(){
        this.Codigo = -1;
    }

    public Evento(int codigo, String nome){
        this.Codigo = codigo;
        this.Nome = nome;
    }
    
    public Evento(String nome){
        this.Codigo = -1;
        this.Nome = nome;
    }

    public int getCodigo(){
        return this.Codigo;
    }
    
    public void setCodigo(int codigo){
        this.Codigo = codigo;
    }
    
    public String getNome(){
        return this.Nome;
    }
    
    public void setNome(String nome){
        this.Nome = nome;
    }
    
    public void Validar() throws Exception{
        if(this.Nome.length() < 5){
            throw new Exception("Erro: O nome deve ter mais que 5 caracteres !");
        }
    }
}
