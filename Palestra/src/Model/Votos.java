package Model;

public class Votos {
    public int Codigo;
    public int Palestra;
    public int Voto;
    
    public Votos(){
        this.Codigo = -1;
    }
    
    public int getCodigo(){
        return this.Codigo;
    }
    public void setCodigo(int codigo){
        this.Codigo = codigo;
    }
    
    public int getPalestra(){
        return this.Palestra;
    }
    public void setPalestra(int palestra){
        this.Palestra = palestra;
    }
    
    public int getVoto(){
        return this.Voto;
    }
    public void setVoto(int voto){
        this.Voto = voto;
    }
}
