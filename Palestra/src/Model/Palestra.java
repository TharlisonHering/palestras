package Model;

public class Palestra {
    public int Codigo;
    public String Nome;
    public String Data;
    public int Evento;

    public Palestra(){
        this.Codigo = -1;
    }
    
    public int getCodigo(){
        return this.Codigo;
    }
    public void setCodigo(int codigo){
        this.Codigo = codigo;
    }
    
    public String getNome(){
        return this.Nome;
    }
    public void setNome(String nome){
        this.Nome = nome;
    }
    
    public String getData(){
        return this.Data;
    }
    public void setData(String data){
        this.Data = data;
    }
    
    public int getEvento(){
        return this.Evento;
    }
    public void setEvento(int evento){
        this.Evento = evento;
    }
    
    public void Validar(){
        
    }
    
    public String DDMMYYYYparaYYYYMMDD(String DataAtual){
        String DataNova, Dia = "", Mes = "", Ano = "";
        int count = 0;
        
        for(int i = 0; i < DataAtual.length(); i++){
            if(DataAtual.charAt(i) =='/'){
                count++;
            }else{
                if(count == 0)
                    Dia += DataAtual.charAt(i);
                if(count == 1 )
                    Mes += DataAtual.charAt(i);
                if(count == 2 )
                    Ano += DataAtual.charAt(i);
            }
        }
        DataNova = Ano+'/'+Mes+'/'+Dia;
        return DataNova;
    }
    
    public String YYYYMMDDparaDDMMYYYY(String DataAtual){
        String DataNova, Dia = "", Mes = "", Ano = "";
        int count = 0;
        
        for(int i = 0; i < DataAtual.length(); i++){
            if(DataAtual.charAt(i) =='/'){
                count++;
            }else{
                if(count == 0)
                    Dia += DataAtual.charAt(i);
                if(count == 1 )
                    Mes += DataAtual.charAt(i);
                if(count == 2 )
                    Ano += DataAtual.charAt(i);
            }
        }
        DataNova = Dia+'/'+Mes+'/'+Ano;
        return DataNova;
    }
}

