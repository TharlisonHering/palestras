/***********************************************************************
*                         BANCO DE DADOS                               *
************************************************************************/

create database talkme;
use talkme;

CREATE TABLE Eventos( 
	Codigo int not null AUTO_INCREMENT, 
	Nome Varchar(50), 
	PRIMARY KEY(Codigo) 
);

create table usuarios( 
	Codigo int not null AUTO_INCREMENT, 
	Nome varchar(100), 
	Email Varchar(30), 
	Senha Varchar(30), 
	PRIMARY KEY(Codigo) 
)

create table Palestrantes( 
    Codigo int not null AUTO_INCREMENT, 
    Nome varchar(100), 
    Email Varchar(30), 
    Telefone Varchar(15),
    Descricao Varchar(255),
    CONSTRAINT Codigo_pk PRIMARY KEY(Codigo) 
);

CREATE TABLE Palestras (
    Codigo int NOT NULL AUTO_INCREMENT,
    Nome varchar(100) NOT NULL,
    Data DATE,
	Evento int not null,
    PRIMARY KEY (Codigo),
    FOREIGN KEY (Evento) REFERENCES Eventos(Codigo)
);

CREATE TABLE PalestrantePalestras (
    Codigo int NOT NULL AUTO_INCREMENT,
    Palestra int not null,
    Palestrante int not null,
	PRIMARY KEY (Codigo),
    FOREIGN KEY (Palestra) REFERENCES palestras(Codigo),
    FOREIGN KEY (Palestrante) REFERENCES palestrantes(Codigo)
);


CREATE TABLE VOTOS(
	CODIGO INT NOT NULL AUTO_INCREMENT,
    PALESTRA INT NOT NULL,
    NOTA INT NOT NULL,
    PRIMARY KEY(CODIGO),
    CONSTRAINT FK_VOTO_PALESTRA FOREIGN KEY(PALESTRA) REFERENCES PALESTRAS(CODIGO)
);


CREATE TABLE RESULTADO(
	Codigo int not null AUTO_INCREMENT,
    Palestra int not null,
    TotalVotos int not null,
    Resultado int not null,
    PRIMARY KEY(Codigo),
    CONSTRAINT Resultado_Palestra FOREIGN KEY (Palestra) REFERENCES palestras(Codigo)
);







